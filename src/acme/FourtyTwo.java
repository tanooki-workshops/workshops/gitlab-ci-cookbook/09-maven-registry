package acme;

import java.lang.String;
import java.lang.System;

public class FourtyTwo {
    public String fullySpelledOut() {
        return "👋 fourty two 😃";
    }

    public void printMe() {
        System.out.println(this.fullySpelledOut());
    }

    public static void main( String[] args )
    {
        FourtyTwo myNumber = new FourtyTwo();
        System.out.println(myNumber.fullySpelledOut());
    }

}

